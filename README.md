# Stage1
**Created by:** Maïwenn Doucet Chardron
**Date:** 22/06/2024
**Version:** 1.0

## Content 

### Directory "interfaceGraphique" contains : 
- **clientMistral**

### Directory "projetClientServeur" contains : 
- **socketClient**
- **socketServeurForkBis** (fork method)
- **socketServeurThread** (thread method)
- **colorLog.sh** (bash script for coloring logs)

