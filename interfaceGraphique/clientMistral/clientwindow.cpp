#include "clientwindow.h"
#include "ui_clientwindow.h"


ClientWindow::ClientWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ClientWindow)
    , networkManager(new QNetworkAccessManager(this))
    , waitingDialog(new QMessageBox(this))
{
    ui->setupUi(this);

    // Configuration de la boîte de dialogue d'attente
    waitingDialog->setWindowTitle("Veuillez patienter");
    waitingDialog->setText("La réponse du serveur est en attente...");
    waitingDialog->setStandardButtons(QMessageBox::NoButton);
    waitingDialog->setIcon(QMessageBox::Information);

    //Connexion des signaux et des slots
    //Connect(sender, signal, receiver, slot)
    connect(ui->sendButton, &QPushButton::clicked, this, &ClientWindow::sendRequest);
    connect(networkManager, &QNetworkAccessManager::finished, this, &ClientWindow::replyToRequest);
}

ClientWindow::~ClientWindow()
{
    delete ui;
}

// Méthode send
void ClientWindow::sendRequest(){
    // Afficher la boîte de dialogue d'attente
    waitingDialog->show();

    // Création de l'objet JSON
    QJsonObject objectJson;
    objectJson["model"] = "mistral-large-latest";
    objectJson["response_format"] = QJsonObject{{"type", "json_object"}};

    // Ajout du message à la liste
    QJsonArray messageArray;
    QJsonObject messageObject;
    messageObject["role"] = "user";
    QString text = ui->requestLineEdit->text();
    messageObject["content"] = text;
    messageArray.append(messageObject);
    objectJson["messages"] = messageArray;

    //Créer un document JSON à partir de notre objet
    QJsonDocument documentJson(objectJson);
    QByteArray jsonData = documentJson.toJson();

    // url http
    QNetworkRequest request(QUrl("https://api.mistral.ai/v1/chat/completions"));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("Accept","application/json");

    // Authentification
    const QString MISTRAL_API_KEY = "hQxtLwCbloNEnDnxphGZObWlqlUWjReV";
    QString concatenated = "Bearer " + MISTRAL_API_KEY;
    request.setRawHeader("Authorization", concatenated.toLocal8Bit());

    // Envoie de la requête
    networkManager->post(request, jsonData);
}

// Méthode pour afficher la réponse
void ClientWindow::replyToRequest(QNetworkReply *reply){
    // Fermer la boîte de dialogue d'attente
    waitingDialog->hide();

    // Récupération de la réponse
        if(reply->error() == QNetworkReply::NoError){
            QByteArray responseData = reply->readAll();
            QJsonDocument jsonResponse = QJsonDocument::fromJson(responseData);
            //QString jsonString = jsonResponse.toJson(QJsonDocument::Indented);
            //ui->responseBox->append(jsonString);

            QJsonObject rootObject = jsonResponse.object();
            if(rootObject.contains("choices")) {
                QJsonArray choice = rootObject["choices"].toArray();
                for(int i = 0; i < choice.size(); i++) {
                    QJsonObject objectChoices = choice[i].toObject();

                    if(objectChoices.contains("message")){
                        QJsonObject objectMessage = objectChoices["message"].toObject();

                        if(objectMessage.contains("content")) {
                            QString content = objectMessage["content"].toString();
                            content.remove("\"");
                            ui->responseBox->append(content);
                        }
                    }
                }
            }
        }
        else {
            QString err = reply->errorString();
            switch(reply->error()) {
            case QNetworkReply::ConnectionRefusedError:
                ui->responseBox->append(tr("La connexion a été refusée."));
                break;
            case QNetworkReply::TimeoutError:
                ui->responseBox->append(tr("La requête a dépassé le délai d'attente."));
                break;
            case QNetworkReply::ProtocolFailure:
                ui->responseBox->append(tr("Une erreur de protocole réseau a été rencontrée."));
                break;
            case QNetworkReply::SslHandshakeFailedError:
                ui->responseBox->append(tr("Un problème avec le certificat SSL/TLS a été détecté."));
                break;
            default:
                ui->responseBox->append(err);
                break;
            }
        }
        reply->deleteLater();
}

