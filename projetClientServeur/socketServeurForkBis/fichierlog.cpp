#include "fichierlog.h"

fichierlog::fichierlog(const std::string& grv, const std::string& zn) {
    temps = currentTime();
    gravite = defGravite(grv);
    zone = zn;
}

std::string fichierlog::currentTime() {
    auto now = std::chrono::system_clock::now();
    time_t now_c = std::chrono::system_clock::to_time_t(now);

    std::ostringstream oss;
    oss << std::put_time(std::localtime(&now_c), "%Y/%m/%d %H:%M:%S");
    return oss.str();
}

void fichierlog::setGravite(const std::string& g) {
    gravite = g;
}

void fichierlog::setZone(const std::string& zn) {
    zone = zn;
}

std::string fichierlog::toString() {
    return temps + " - " + " [" + gravite + "] " + " - " + zone + " - ";
}

std::string fichierlog::defGravite(const std::string& grv) {
    const std::string ERA_EMERGENCY = "Emergency";
    const std::string ERA_ALERT = "Alert";
    const std::string ERA_CRITICAL = "Critical";
    const std::string ERA_ERROR = "Error";
    const std::string ERA_WARNING = "Warning";
    const std::string ERA_NOTICE = "Notice";
    const std::string ERA_INFO = "Info";
    const std::string ERA_DEBUG = "Debug";

    if (grv == ERA_EMERGENCY) {
        setGravite(ERA_EMERGENCY);
    } else if (grv == ERA_ALERT) {
        setGravite(ERA_ALERT);
    } else if (grv == ERA_CRITICAL) {
        setGravite(ERA_CRITICAL);
    } else if (grv == ERA_ERROR) {
        setGravite(ERA_ERROR);
    } else if (grv == ERA_WARNING) {
        setGravite(ERA_WARNING);
    } else if (grv == ERA_NOTICE) {
        setGravite(ERA_NOTICE);
    } else if (grv == ERA_INFO) {
        setGravite(ERA_INFO);
    } else if (grv == ERA_DEBUG)
    {
        setGravite(ERA_DEBUG);
    }
    else
    {
        setGravite("non déterminée");
    }

    return gravite; // Retourne la gravité mise à jour
}

void fichierlog::writeLog(const std::string& message) {
    std::string const nomFichier("/home/maiwenn/logs.txt");
    std::ofstream monFlux(nomFichier.c_str(), std::ios::app);

    if (monFlux) {
        monFlux << toString() << message << std::endl;
    } else {
        std::cerr << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
    }
}
