TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        fichierlog.cpp \
        main.cpp

HEADERS += \
    fichierlog.h
