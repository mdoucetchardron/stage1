#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>

using namespace std;

/*
 * sizeof -> permet d'obtenir la taille en octets d'un type ou d'une variable
 * socklen_t -> représente la taille des structures d'adresses de sockets - assure que les fonctions de la bibliothèque de sockets, comprennent correctement la taille des structures d'adresses
 */

#define CONNECTION_HOST "127.0.0.1"
#define LISTENING_PORT 12345
#define BUFFER_SIZE 1024
#define END "q"

int main()
{
    //Création du socket
    int socketFD = socket(AF_INET, SOCK_STREAM,0);

    if(socketFD == -1)
    {
        perror("socket failed");
        exit(1);
    }

    //Configuration et liaison du socket
    struct sockaddr_in socketAddress;
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_port = htons(LISTENING_PORT);


    int inetReturnCode = inet_pton(AF_INET, CONNECTION_HOST, &socketAddress.sin_addr);

    if(inetReturnCode == -1)
    {
        perror("invalid address");
    }

    //Connexion au serveur
    socklen_t socketAddressLength = sizeof(socketAddress);
    int connectionStatus = connect(socketFD, (struct sockaddr*)&socketAddress, socketAddressLength);

    if (connectionStatus == -1)
    {
        perror("connection failed");
        close(socketFD);
        exit(1);
    }

    //boucle do-while
    char buffer[BUFFER_SIZE] = {0};
    int sentBytes;
    int receivedBytes;

    do {
        // Envoi du message
        memset(buffer, 0, BUFFER_SIZE);
        cin.getline(buffer, BUFFER_SIZE);
        sentBytes = send(socketFD, buffer, strlen(buffer), 0);

        if(sentBytes == -1) {
            perror("(CLIENT)send failed");
            close(socketFD);
            exit(1);
        }

        if(strcmp(buffer, END) == 0) {
            break;
        }

        // Réception du message
        memset(buffer, 0, BUFFER_SIZE);
        receivedBytes = recv(socketFD, buffer, BUFFER_SIZE, 0);

        if(receivedBytes < 0) {
            perror("recv failed");
            close(socketFD);
            exit(1);
        }
        cout << "Serveur : " << buffer << endl;

    } while(strcmp(buffer, END)!= 0);

    //Fermeture du socket client
    close(socketFD);

    return 0;
}
