#ifndef FICHIERLOG_H
#define FICHIERLOG_H

#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <pthread.h>

extern pthread_mutex_t logMutex; // Déclaration du mutex

/**
 * Création classe fichier logs 05/06/2024
 * @brief The fichierLog class
 */
class fichierlog {

private:
    std::string temps;
    std::string gravite;
    std::string zone;

public:
    //Déclaration du constructeur
    fichierlog(const std::string& grv, const std::string& zn);

    //Déclaration des fonctions membres
    std::string currentTime();
    void setGravite(const std::string& g);
    void setZone(const std::string& zn);
    std::string toString();
    std::string defGravite(const std::string& grv);
    void writeLog(const std::string& message);
};

#endif // FICHIERLOG_H
