#include <fichierlog.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <sstream>

#define LISTENING_PORT 12345
#define PENDING_QUEUE_MAXLENGTH 2
#define BUFFER_SIZE 1024
#define END "q"

using namespace std;

void* handle_client(void* arg) {
    int connectedSocketFD = *(int*)arg;
    char buffer[BUFFER_SIZE] = {0};
    int sentBytes;
    int receivedBytes;
    fichierlog log ("Client", "Info");

    do {
        memset(buffer, 0, BUFFER_SIZE);
        receivedBytes = recv(connectedSocketFD, buffer, BUFFER_SIZE, 0);

        if(receivedBytes < 0) {
            perror("recv failed");
            log.setGravite("Error");
            log.writeLog("recv failed");
            close(connectedSocketFD);
            pthread_exit(NULL);
        }
        printf("Client : %s\n", buffer);

        if(strcmp(buffer, END) == 0) {
            break;
        }

        memset(buffer, 0, BUFFER_SIZE);
        fgets(buffer, BUFFER_SIZE, stdin);
        sentBytes = send(connectedSocketFD, buffer, strlen(buffer), 0);

        if(sentBytes == -1) {
            perror("(SERVEUR)send failed");
            log.setZone("Serveur");
            log.setGravite("Error");
            log.writeLog("send failed");
            close(connectedSocketFD);
            pthread_exit(NULL);
        }
    } while(strcmp(buffer, END)!= 0);

    printf("Client déconnecté.\n");
    log.setZone("Client");
    log.setGravite("Warning");
    log.writeLog("Client déconnecté.");
    close(connectedSocketFD);
    pthread_exit(NULL);
}

int main()
{
    //Création log
    fichierlog log("Info","Serveur");

    // Création du socket serveur
    int socketFD = socket(AF_INET, SOCK_STREAM, 0);

    if(socketFD < 0)
    {
        perror("socket failed"); // Affichage du message d'erreur
        log.setGravite("Error");
        log.writeLog("socket failed");
        exit(EXIT_FAILURE); // On sort en indiquant un code erreur
    }
    cout << "Socket created successfully." << endl;
    log.writeLog("Socket created successfully.");

    // Optimisation de la réutilisation du socket
    int enableReuse = 1;
    if (setsockopt(socketFD, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(int)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
        log.setGravite("Error");
        log.writeLog("setsockopt(SO_REUSEADDR) failed");
        close(socketFD);
        exit(EXIT_FAILURE);
    }

    // Configuration de l'adresse de socket
    struct sockaddr_in socketAdress;
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = htons(LISTENING_PORT);
    socketAdress.sin_addr.s_addr = INADDR_ANY; //Ecoute sur toutes les interfaces

    // Liaison de la socket à l'adresse et au port spécifié
    int socketAddressLength = sizeof(socketAdress);
    int bindReturnCode = bind(socketFD, (struct sockaddr*)&socketAdress, socketAddressLength);

    if(bindReturnCode == -1)
    {
        perror("bind failed");
        log.setGravite("Error");
        log.writeLog("bind failed");
        exit(EXIT_FAILURE);
    }
    cout << "Bind successful." << endl;
    log.writeLog("Bind successful.");

    // Ecoute du socket serveur (attente de nouvelles connexions)
    if(listen(socketFD, PENDING_QUEUE_MAXLENGTH) == -1)
    {
        perror("listen failed");
        log.setGravite("Error");
        log.writeLog("listen failed");
        exit(EXIT_FAILURE);
    }
    cout << "Server is now listening." << endl;
    log.writeLog("Server is now listening.");
    std::cout << "En attente de nouvelles connexions ..." << std::endl;

    // Accèpter connexion pour chaque client
    int nbClient = 0;

    while(true){
            int connectedSocketFD = accept(socketFD, (struct sockaddr*)&socketAdress, (socklen_t *)&socketAddressLength);
            if(connectedSocketFD == -1)
            {
                perror("accept failed");
                log.setGravite("Error");
                log.writeLog("Accept failed");
                close(connectedSocketFD);
                close(socketFD);
                exit(EXIT_FAILURE);
            }
            else
            {
                nbClient++;
                cout << "Connection accepted with client " << nbClient << endl;
                log.writeLog("Connection accepted with client " + to_string(nbClient));
            }

            //Création d'un thread pour chaque client
            pthread_t thread;
            pthread_create(&thread, NULL, handle_client, &connectedSocketFD);
            // Attente de la fin du thread avant de continuer
            pthread_detach(thread);
    }
    // Destruction du mutex
    pthread_mutex_destroy(&logMutex);

    // Fermetures des sockets
    close(socketFD);
    return 0;
}
/*
     * // Réception d'un message
     * char buffer[BUFFER_SIZE] = {0};
     * int receivedBytes = recv(connectedSocketFD, buffer, BUFFER_SIZE, 0);
     *
     * if(receivedBytes < 0)
     * {
     *     perror("recv failed");
     *     close(connectedSocketFD);
     *     close(socketFD);
     *     exit(1);
     * }
     *
     * std::cout << "Données reçues : " << buffer << std::endl;
     *
     * // Envoi d'un message
     * const char message[] = "(SERVEUR)Bonjour client, je suis le serveur !";
     * int sentBytes = send(connectedSocketFD, message, strlen(message), 0);
     *
     * if(sentBytes == -1)
     * {
     *     perror("(SERVEUR)send failed");
     *     close(connectedSocketFD);
     *     close(socketFD);
     *     exit(1);
     * }
    */
