#!/bin/bash

# Chemin vers le fichier de logs
LOG_FILE=$1

# Lecture des lignes du fichier de logs
while IFS= read -r line; do
    # Extraire la date
    date_part=$(echo "$line" | grep -oP '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}')

    # Extraire le reste de la ligne (le log)
    log_part=$(echo "$line" | sed -E 's/^\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}//')

    # Appliquer des couleurs à la date
    date_colored="\e[1;35m$date_part\e[0m"  # Violet gras

    # Appliquer des couleurs au log
    if [[ $log_part == *"Emergency"* ]]; then
        log_colored="\e[1;31m$log_part\e[0m"  # Rouge gras
    elif [[ $log_part == *"Alert"* ]]; then
        log_colored="\e[1;31m$log_part\e[0m"  # Rouge gras
    elif [[ $log_part == *"Critical"* ]]; then
        log_colored="\e[1;31m$log_part\e[0m"  # Rouge gras
    elif [[ $log_part == *"Error"* ]]; then
        log_colored="\e[1;31m$log_part\e[0m"  # Rouge gras
    elif [[ $log_part == *"Warning"* ]]; then
        log_colored="\e[1;33m$log_part\e[0m"  # Jaune gras
    elif [[ $log_part == *"Notice"* ]]; then
        log_colored="\e[1;34m$log_part\e[0m"  # Bleu gras
    elif [[ $log_part == *"Info"* ]]; then
        log_colored="\e[1;32m$log_part\e[0m"  # Vert gras
    elif [[ $log_part == *"Debug"* ]]; then
        log_colored="\e[1;36m$log_part\e[0m"  # Cyan gras
    else
        log_colored="$log_part"  # Pas de couleur
    fi

    # Afficher la ligne avec les couleurs
    echo -e "$date_colored $log_colored"
done < "$LOG_FILE"
